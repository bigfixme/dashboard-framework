Content for this project involves multiple sub-projects.

DashboardDeveloper - is C# code for creating a simple utility for use within Notepad++ or at the command line.

Documentation - This a collection of various documents that will help you with developing your dashboards.  Start with the Advanced Development Guide

Examples - This is a collection of various dashboards demonstrating several techniques and uses for dashboard functions and UI elements.

Libraries - These are the flattened individual libraries you can copy/paste into your dashboard project folder and include in your dashboard ojo file.  These libraries have been tested and validated functional within the BigFix Console context.

Maintenance Windows - This is a dashboard that, once it is complete, will be published to the BigFix Labs for use by anyone.  I wanted to keep a copy of the source here so everyone can see it develop over time and have a real-live example of using the Dashboard Framework in their projects.

Raw Libraries - Whenever possible, the raw libraries (prior to flattening) are included within this directory.  As well as URL links to each of the libraries.

Ubuntu Packages - This is another dashboard I've built using the framework.  It is used to inventory available and installed packages on your Ubuntu endpoints using the apt-get and dpkg utilities.  Then allows you to install/remove those packages with a click of a button.  You can even create fixlets for modification and customization.

bigfix.me - Several dashboards that will be included in an upcoming 3rd party sites project.
