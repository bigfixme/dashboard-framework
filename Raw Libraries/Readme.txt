This folder contains the raw downloaded libraries. 

In many cases we need to flatten out the library for inclusion in a custom site.  Therefore we make slight modifications to the libraries that mostly involve directory paths.

The modified versions of these libraries are included under the Libraries folder.