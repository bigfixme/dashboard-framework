Libraries can easily be included in your projects by adding a simple xxx.include file into your projects directory.  

See the example dashboard for use case.


This folder contains the modified versions of downloaded libraries. 

In many cases we need to flatten out the library for inclusion in a custom site.  Therefore we make slight modifications to the libraries that mostly involve directory paths.

The modified versions of these libraries are included under the Raw Libraries folder.