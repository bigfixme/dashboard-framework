﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Threading;
using System.Runtime.InteropServices;
using System.IO;
using System.Xml;
using REST;
using REST.BES_Namespace;
using REST.BESAPI_Namespace;
using System.ComponentModel;
using System.Xml.Serialization;

namespace DashboardDeveloper
{
    
    public class Settings
    {
        public bool askForCredentials = true;
        public bool UserCanceledDialog = false;

        

        private bool urlValid = false;
        private string _Url;
        public string Url
        {
            get { return _Url; }
            set
            {
                Uri myUri;
                if (Uri.TryCreate(value, UriKind.Absolute, out myUri))
                {
                    if (!value.ToLower().Trim().EndsWith("api"))
                    {
                        if (value.ToLower().Trim().EndsWith("api/"))
                            _Url = value.ToLower().Trim().Replace("http://","https://").TrimEnd('/');
                        else 
                            if (value.ToLower().Trim().EndsWith("/"))
                                _Url = value.ToLower().Trim().Replace("http://", "https://") + "api";
                            else _Url = value.ToLower().Trim().Replace("http://", "https://") + "/api";
                    }
                    else _Url = value.Replace("http://", "https://");

                    urlValid = true;
                }
                else
                {
                    _Url = value.ToLower().Trim().Replace("http://", "https://");
                    urlValid = false;
                }

                //validate values
                if (!urlValid || string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
                    askForCredentials = true;
                else askForCredentials = false;
            }
        }



        private string _Username;
        public string Username
        {
            get { return _Username; }
            set
            {
                _Username = value;

                //validate values
                if (!urlValid || string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
                    askForCredentials = true;
                else askForCredentials = false;
            }
        }

        private string _Password;
        public string Password
        {
            get { return _Password; }
            set
            {
                _Password = value;

                //validate values
                if (!urlValid || string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
                    askForCredentials = true;
                else askForCredentials = false;
            }
        }
        
        
        
        
        
        public bool saveSettings = false;

        private static XmlDocument _doc = new XmlDocument();
        private static readonly object Locker = new object();

        public void PromptforSettings()
        {
            frmCredentials cred = new frmCredentials();
            cred.settings = this;

            cred.setUrl(Url);
            cred.setUsername(Username);
            cred.setPassword(Password);
            cred.setSave(saveSettings);

            cred.ShowDialog();
        }


        public void Read(string fileName)
        {
            using (System.IO.StreamReader myFile = new System.IO.StreamReader(fileName))
            {
                string myXml = myFile.ReadToEnd();

                _doc.LoadXml(myXml);

                foreach (XmlNode x1node in _doc.ChildNodes)
                {
                    foreach (XmlNode x2node in x1node.ChildNodes)
                    {
                        if (x2node.Name.ToLower() == "restapi")
                        {
                            Url = x2node.FirstChild.InnerText;
                        }
                        else if (x2node.Name.ToLower() == "credential")
                        {
                            foreach (XmlNode x3node in x2node.ChildNodes)
                            {
                                if (x3node.Name.ToLower() == "username")
                                    Username = x3node.InnerText;
                                else if (x3node.Name.ToLower() == "password")
                                {
                                    Crypto3DES crypt = new Crypto3DES();
                                    Password = crypt.Decrypt3DES(x3node.InnerText);                                    
                                }
                            }
                        }
                    }
                }
            }            
        }

        public void Save(string fileName)
        {
            if (File.Exists(fileName))
                File.Delete(fileName);
            
            var root = _doc.CreateElement("settings");
            _doc.AppendChild(root);
            

            lock (Locker)
            {
                var rest = (XmlElement)_doc.DocumentElement.AppendChild(_doc.CreateElement("restapi"));

                //el.SetAttribute("TimeStamp", DateTime.Now.ToUniversalTime().ToString("MM/dd/yyyy HH:mm:ss tt"));
                rest.AppendChild(_doc.CreateElement("url")).InnerText = Url.Trim();


                var cred = (XmlElement)_doc.DocumentElement.AppendChild(_doc.CreateElement("credential"));

                //el.SetAttribute("TimeStamp", DateTime.Now.ToUniversalTime().ToString("MM/dd/yyyy HH:mm:ss tt"));
                cred.AppendChild(_doc.CreateElement("username")).InnerText = Username.Trim();

                Crypto3DES crypt = new Crypto3DES();
                cred.AppendChild(_doc.CreateElement("password")).InnerText = crypt.Encrypt3DES(Password.Trim());

                _doc.Save(fileName);
            }

        }

    }




















    class Program
    {
        static Mutex SingleInstanceMutex = null;
        static public string ojoDir = null;
        static Settings settings = new Settings();
        static public Credentials creds = new Credentials();
            
        static public bool isFailed = false;

        static int debugLevel = 10;
        static int keepLogs = 15;
        static public DateTime startTime = DateTime.Now.ToUniversalTime();

        static public bool areClientFiles = false;


        static public void Main(string[] args)
        {
#if DEBUG
            settings.Url = "https://lab.bigfix.me:52311";
            settings.Username = "BigFixAdmin";
            settings.Password = "[password]";
            //this information is passed in by the server plugin controller
            ojoDir = @"C:\Users\Daniel\bitbucket.org\Dashboard Framework\bigfix.me";
#else
            if (args.Length > 0)
                ojoDir = args[0];

            if (args.Length > 1)
            {
                if (args[1].ToLower().Trim() == "/clientfiles")
                    areClientFiles = true;
                else settings.Url = args[1];
            }
            if (args.Length > 2)
                settings.Username = args[2];
            if (args.Length > 3)
                settings.Password = args[3];
#endif

            



            if (ojoDir != null)
            {
                if (!ojoDir.EndsWith(@"\"))
                    ojoDir += @"\";
            }

            //========================================================================================================================
            //OUTPUT HEADER FOR CONSOLE USER
            Log.Add("-------------------------------------------------------------------------------", Log.Stack(),startTime);
            Log.Add("DashboardDeveloper v" + Assembly.GetExecutingAssembly().GetName().Version.ToString(), Log.Stack(), startTime);
            Log.Add("  For more information email me at danielheth@bigfix.me", Log.Stack(), startTime);
            Console.WriteLine("");
            Log.Add("  Processing the following incoming arguments:  ", Log.Stack(), startTime);

            Log.Add("         Url:  " + settings.Url, Log.Stack(), startTime);
            Log.Add("    Username:  " + settings.Username, Log.Stack(), startTime);

            Log.Add("    Processing the following incoming arguments:  ", Log.Stack(), startTime);

            

            if (string.IsNullOrEmpty(ojoDir))
            {
                Log.Add("    Directory: null", Log.Stack(), startTime);
                Log.Add("-------------------------------------------------------------------------------", Log.Stack(), startTime);
                Console.WriteLine("");

                Log.Add("ERROR:  Invalid Directory Specified", Log.Stack(), startTime, true);
                Exiting("Invalid Arguments");
                return;
            }
            else
            {
                Log.Add("    Directory: " + ojoDir, Log.Stack(), startTime);
                Log.Add("-------------------------------------------------------------------------------", Log.Stack(), startTime);
                Console.WriteLine("");
            
                if (!Directory.Exists(ojoDir))
                {
                    Log.Add("ERROR:  Directory Does Not Exist", Log.Stack(), startTime, true);
                    Exiting("Invalid Arguments");
                    return;
                }
            }

            
            



            //now we need credentials!
            string settingsFile = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\settings.xml";
            if (File.Exists(settingsFile))
                settings.Read(settingsFile);
            while (settings.askForCredentials && !settings.UserCanceledDialog)
            {
                settings.PromptforSettings();
                if (settings.saveSettings)
                {
                    settings.Save(settingsFile);
                }
            }
            if (settings.UserCanceledDialog)
            {
                Log.Add("User Canceled Settings Dialog!", Log.Stack(), startTime);
                Exiting("Unable to Proceed!");
                return;
            }

            creds.Username = settings.Username;
            creds.Password = settings.Password;
            creds.Url = settings.Url;


            //========================================================================================================================
            //PREVENT MORE THAN ONE INSTANCE OF THIS PROGRAM FROM RUNNING AT THE SAME TIME
            string appGuid = ((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(GuidAttribute), false).GetValue(0)).Value.ToString();
            string mutexId = string.Format("Global\\{{{0}}}", appGuid);
            try
            {
                SingleInstanceMutex = Mutex.OpenExisting(mutexId);
                Log.Add("WARNING:  This application is already running!" + Environment.NewLine + "Exiting Application", Log.Stack(), startTime);
                
                return;
            }
            catch (Exception ex)
            {
                SingleInstanceMutex = new Mutex(true, mutexId);
            }





            //========================================================================================================================
            //CREATE CUSTOM SITE FOR THIS DASHBOARD IF NEEDED...
            bool createdSite = false;  //used later to skip file checking step
            string siteName = Path.GetFileName( ojoDir.TrimEnd('\\') );

            Log.Add("Validating Custom Site:  " + siteName, Log.Stack(), startTime);
            
            //does this site exist?
            RESTResult rr = RESTAPI.getREST("/api/site/custom/" + siteName, creds);
            RESTResult _rr;
            if (rr.isError)
            {
                Log.Add("Creating new Custom Site:  " + siteName, Log.Stack(), startTime);
                createdSite = true;

                //we need to create this custom site before we can proceed

                Site besSite = new Site();
                besSite.Name = siteName;
                besSite.Description = "Created automatically by Dashboard Developer for " + settings.Username + ".";

                BES bes = new BES();
                bes.Items = new object[] { besSite };
                bes.ItemsElementName = new ItemsChoiceType2[] { ItemsChoiceType2.CustomSite };

                _rr = RESTAPI.postREST("/api/sites", creds, bes);
                if (!_rr.isError)
                {
                    Log.Add("Successfully created Custom Site!", Log.Stack(), startTime);
                }
                else
                {
                    Log.Add("Failed to create new Custom Site!" + Environment.NewLine + _rr.ErrorMessage, Log.Stack(), startTime, true);
                    Exiting("Unable to Proceed!");
                    return;
                }
            }
            else
            {
                Log.Add("Custom Site already exists!" + Environment.NewLine + rr.ErrorMessage, Log.Stack(), startTime);
                createdSite = false;
            }











            //========================================================================================================================
            //IMPORT FILES IN THIS DIRECTORY INTO THE CUSTOM SITE
            List<BESAPISiteFile> siteFiles = new List<BESAPISiteFile>();









            //retrieve a list of all files in this directory
            List<string> fileNames = new List<string>();
            string[] tmpfileNames;
            string[] libfileNames;
            try
            {
                //1. retrieve a list of all files in the list and we'll need to look at each one to see if we have any "include" files.
                tmpfileNames = Directory.GetFiles(Path.GetDirectoryName(ojoDir));

                foreach (string fileName in tmpfileNames)
                {
                    if (fileName.ToLower().EndsWith(".include")) //these are special files to include libraries
                    { //within this file is a relative path to the library we need to include.
                        
                        
                        using (System.IO.StreamReader file = new System.IO.StreamReader(fileName))
                        {
                            string libPath;
                            while ((libPath = file.ReadLine()) != null)
                            {
                                if (!libPath.Trim().StartsWith("//") && !string.IsNullOrEmpty(libPath.Trim())) //ignore comments and blank lines
                                {
                                    if (libPath.Trim().Contains("..\\"))  //relative path to another directory... convert to full path format
                                    {
                                        string d = Path.GetDirectoryName(ojoDir);
                                        while (libPath.StartsWith("..\\"))
                                        {
                                            d = Path.GetDirectoryName(d);
                                            libPath = libPath.Substring(2, libPath.Length - 2);
                                        }
                                        libPath = d + libPath;
                                    }
                                    
                                    if (Directory.Exists(libPath))  //this is an entire directory to include
                                    {
                                        libfileNames = Directory.GetFiles(libPath);
                                        foreach (string libfileName in libfileNames)
                                        {
                                            //don't include demo.html or demo.htm files automatically.  To include them, call that specific file out.
                                            if (!libfileName.ToLower().EndsWith("demo.html") && !libfileName.ToLower().EndsWith("demo.htm"))
                                                fileNames.Add(libfileName);
                                        }
                                    }
                                    else if (File.Exists(libPath)) //then this is a single file to include... 
                                    {
                                        fileNames.Add(libPath);
                                    }

                                }

                            }
                        }

                    }
                    else
                    {
                        fileNames.Add(fileName); //if not an include file, then just add it
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Add("Can not to read local directory:  " + ex.Message, Log.Stack(), startTime, true);
                Exiting("Unable to Proceed!");
                return;
            }




















            //retrieve a list of files currently in the site
            rr = RESTAPI.getREST("/api/site/custom/" + siteName + "/files", creds);
            if (!rr.isError)
            {
                if (rr.besapi != null)
                {
                    if (rr.besapi.Items != null)
                    {
                        foreach (BESAPISiteFile siteFile in rr.besapi.Items)
                        {
                            //ONLY ADD FILES THAT EXIST LOCALLY
                            //find this file in the local inventory....
                            bool foundit = false;
                            foreach (string file in fileNames)
                            {
                                if (file.ToLower().EndsWith(siteFile.Name.ToLower()))
                                {
                                    foundit = true;
                                }
                            }


                            if (foundit)
                                siteFiles.Add(siteFile);
                            else //we need to delete it from the site before proceeding
                            {
                                _rr = RESTAPI.deleteREST(siteFile.Resource, creds);
                                if (!_rr.isError)
                                {
                                    Log.Add("Successfully removed site File: " + siteFile.Name, Log.Stack(), startTime);
                                }
                                else
                                {
                                    Log.Add("Warning:  Unable to delete existing siteFile (" + siteFile.Name + ")" + Environment.NewLine + _rr.ErrorMessage, Log.Stack(), startTime);
                                }
                            }
                        }
                    } //if (rr.besapi.Items != null)
                } //if (rr.besapi != null)
            }
            else
            {
                Log.Add("ERROR:  Failed to retrieve Site Files List!", Log.Stack(), startTime, true);
                Exiting("Unable to Proceed!");
                return;
            }











            
            
            //======================================================================
            bool addFile = true;
            foreach (string fileName in fileNames)
            {   
                addFile = true;
            
                if (!createdSite) //then we need to get information on the existing file if it exists...
                {
                    FileInfo localFile = new FileInfo(fileName); //retrieve additional information about the local file
                    DateTime lastWriteTime = localFile.LastWriteTime.ToUniversalTime();

                    //loop through all siteFiles to find this specific file... then match up properties
                    foreach (BESAPISiteFile siteFile in siteFiles)
                    {
                        if (localFile.Name.ToLower() == siteFile.Name.ToLower())
                        {
                            DateTime lastModified = DateTime.Parse(siteFile.LastModified.Replace("+0000", "").Trim());
                            long localLength = GetFileSizeOnDisk(fileName);

                            //we must remove ojo files and add them again in order to 
                            if (
                                (
                                 (long.Parse(siteFile.FileSize) < localFile.Length - 1) ||
                                 (long.Parse(siteFile.FileSize) > localFile.Length + 1)
                                ) || (lastWriteTime > lastModified))
                            {
                                Log.Add("Update File:  " + Path.GetFileName(fileName) + " (" + siteFile.Resource + ")", Log.Stack(), startTime);
                                

                                if (siteFile.Name.ToLower().EndsWith(".ojo")) //we don't care if it is same or different, we must remove it and readd it every time.
                                {
                                    Log.Add("Removing older OJO.", Log.Stack(), startTime);
                                    _rr = RESTAPI.deleteREST(siteFile.Resource, creds);
                                    if (!_rr.isError)
                                    {
                                        Log.Add("Successfully removed OJO file from site.", Log.Stack(), startTime);
                                        addFile = true; //just flag to add the file back in.
                                    }
                                    else
                                    {
                                        Log.Add("ERROR:  Unable to remove OJO file!   " + siteFile.Resource + Environment.NewLine + _rr.ErrorMessage, Log.Stack(), startTime, true);

                                        //Exiting("Unable to Proceed!");
                                        //return;
                                    }
                                }
                                else
                                {
                                    //try to "update" the file... but since there is a bug we know this won't work.  Leaving code in place in case it is fixed at some later point.
                                    _rr = RESTAPI.postREST(siteFile.Resource, creds, fileName);
                                    if (!_rr.isError)
                                    {
                                        addFile = false;
                                        Log.Add("Successfully updated Site File!", Log.Stack(), startTime);
                                    }
                                    else
                                    {
                                        Log.Add("Removing older Site File: " + siteFile.Name, Log.Stack(), startTime);
                                        _rr = RESTAPI.deleteREST(siteFile.Resource, creds);
                                        if (!_rr.isError)
                                        {
                                            Log.Add("Successfully removed older Site File!", Log.Stack(), startTime);
                                            addFile = true; //just flag to add the file back in.
                                        }
                                        else
                                        {
                                            Log.Add("Failed to removed older Site File!", Log.Stack(), startTime, true);
                                            Exiting("Unable to Proceed!");
                                            return;
                                        }
                                    } //if (!_rr.isError)
                                } //if (siteFile.Name.ToLower().EndsWith(".ojo"))
                            } //if ( (localFile.Length != long.Parse(siteFile.FileSize)) || (lastWriteTime > lastModified) )
                            else
                            {
                                Log.Add("No changes required:  " + siteFile.Name, Log.Stack(), startTime);
                                addFile = false; //file does not need updating... 
                            }
                            break; //found it, no need to continue
                        }
                    }
                }
                
                if (addFile)
                {
                    if (fileName.ToLower().EndsWith(".ojo")) //we don't care if it is same or different, we must remove it and readd it every time.
                        Log.Add("Adding newer OJO:  " + Path.GetFileName(fileName), Log.Stack(), startTime);
                    else Log.Add("Adding File:  " + Path.GetFileName(fileName), Log.Stack(), startTime);
                    

                    _rr = RESTAPI.postREST("/api/site/custom/" + siteName + "/files", creds, fileName);
                    if (!_rr.isError)
                    {
                        Log.Add("Successfully added new Site File!", Log.Stack(), startTime);
                    }
                    else
                    {
                        Log.Add("Failed to add new Site File!", Log.Stack(), startTime, true);
                        Exiting("Unable to Proceed!");
                        return;
                    }

                }
            }


                        

            //========================================================================================================================





            Exiting("Dashboard Developer Finished Processing!");
        }



        static private string getXML(object obj)
        {
            //convert object to xml
            XmlSerializer x = new XmlSerializer(obj.GetType());
            StringWriter textWriter = new StringWriter();
            x.Serialize(textWriter, obj);
            string xml = textWriter.ToString();
            return xml;
        }









        /// <summary>
        /// provide a common user footer for when the application closes.
        /// </summary>
        static private void Exiting(string msg = null)
        {

            //Console.ReadKey();

            if (!string.IsNullOrEmpty(msg))
                Log.Add(msg, Log.Stack(), startTime);


            //========================================================================================================================
            //Disable - PREVENT MORE THAN ONE INSTANCE OF THIS PROGRAM FROM RUNNING AT THE SAME TIME

            try
            {
                if (SingleInstanceMutex != null)
                    SingleInstanceMutex.ReleaseMutex();
            }
            catch (Exception ex)
            {
                Log.Add("Error:  " + ex.Message, Log.Stack(), startTime, true);
            }


            


#if DEBUG
            Console.WriteLine(Environment.NewLine + "-------------------------------------------------------------------------------");
            Console.WriteLine("Please press any key to continue... " + Environment.NewLine + "Waiting for any key before releasing Mutex and Closing application.");
            Console.ReadKey();
#else
            if (isFailed)
            {
                Console.WriteLine(Environment.NewLine + "-------------------------------------------------------------------------------");
                Console.WriteLine("Please press any key to continue...");
                Console.ReadKey();
            }
#endif
        }













        public static long GetFileSizeOnDisk(string file)
        {
            FileInfo info = new FileInfo(file);
            uint dummy, sectorsPerCluster, bytesPerSector;
            int result = GetDiskFreeSpaceW(info.Directory.Root.FullName, out sectorsPerCluster, out bytesPerSector, out dummy, out dummy);
            if (result == 0) throw new Win32Exception();
            uint clusterSize = sectorsPerCluster * bytesPerSector;
            uint hosize;
            uint losize = GetCompressedFileSizeW(file, out hosize);
            long size;
            size = (long)hosize << 32 | losize;
            return ((size + clusterSize - 1) / clusterSize) * clusterSize;
        }

        [DllImport("kernel32.dll")]
        static extern uint GetCompressedFileSizeW([In, MarshalAs(UnmanagedType.LPWStr)] string lpFileName,
           [Out, MarshalAs(UnmanagedType.U4)] out uint lpFileSizeHigh);

        [DllImport("kernel32.dll", SetLastError = true, PreserveSig = true)]
        static extern int GetDiskFreeSpaceW([In, MarshalAs(UnmanagedType.LPWStr)] string lpRootPathName,
           out uint lpSectorsPerCluster, out uint lpBytesPerSector, out uint lpNumberOfFreeClusters,
           out uint lpTotalNumberOfClusters);


    }
}
