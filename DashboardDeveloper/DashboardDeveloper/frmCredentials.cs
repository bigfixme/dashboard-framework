﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DashboardDeveloper
{
    public partial class frmCredentials : Form
    {
        public Settings settings;


        public frmCredentials()
        {
            InitializeComponent();
        }

        public void setUrl(string value)
        {
            textBox3.Text = value;
        }

        public void setUsername(string value)
        {
            textBox1.Text = value;
        }

        public void setPassword(string value)
        {
            textBox2.Text = value;
        }

        public void setSave(bool value)
        {
            checkBox1.Checked = value;
        }



        private void button2_Click(object sender, EventArgs e)
        {
            settings.UserCanceledDialog = true;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            settings.Url = textBox3.Text;
            settings.Username = textBox1.Text;
            settings.Password = textBox2.Text;

            settings.saveSettings = checkBox1.Checked;

            this.Close();
        }
    }
}
